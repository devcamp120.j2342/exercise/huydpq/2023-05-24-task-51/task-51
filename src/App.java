import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class App {
    public static void main(String[] args) throws Exception {

        // task 1: Tìm và in ra các ký tự xuất hiện nhiều hơn một lần trong String cho trước không phân biệt chữ hoa hay chữ thường. Nếu các ký tự trong chuỗi đều là duy nhất thì xuất ra “NO”


        String str = "Java";
        String str1 = "Devcamp JAVA exercise";
        String str1_1 = "Devcamp";
        System.out.println(task1(str));
        System.out.println(task1(str1));
        System.out.println(task1(str1_1));

        // task 2: Kiểm tra 2 chuỗi có là đảo ngược của nhau hay không. Nếu có xuất ra “OK” ngược lại “KO”
        String str2 = "word";
        String str2_1 = "drow";
        String str2_2 = "java";
        String str2_3 = "js";
        System.out.println("Task 2:");
        task2(str2, str2_1);
        task2(str2_2, str2_3);

        //stask 3: Tìm ký tự chỉ xuất hiện một lần trong chuỗi, nếu có nhiều hơn một thì xuất ra màn hình ký tự đầu tiên. Nếu không có ký tự nào unique xuất ra “NO”.
        System.out.println("Task 3:");
        String str3 = "Devcamp";
        String str31 = "HaHa";
        String resultT3 = findFirstUniqueCharacter(str3);
        System.out.println("Ket qua cua chuoi " + str3 + " la: " + resultT3);
        String resultT31 = findFirstUniqueCharacter(str31);
        System.out.println("Ket qua cua chuoi " + str31 + " la: " + resultT31);

        // task 4: Đảo ngược chuỗi
        String str4 = "word";
        StringBuffer a = new StringBuffer(str4);
        System.out.println("Task 4: "+ a.reverse());

        // task 5: Kiểm tra một chuỗi có chứa chữ số hay không, nếu có in ra false ngược lại true.
        String str5 = "a1bc";
        String str5_1 = "abc";
        System.out.println("Task 4");
        System.out.println(task5(str5));
        System.out.println(task5(str5_1));
        // task 6: Đếm số lượng ký tự nguyên âm xuất hiện trong chuỗi

        String ip6 = "java";
        task6(ip6);

        //task 7: Chuyển chuỗi số nguyên sang int value.
        String numberString = "1234";
        int number = Integer.parseInt(numberString);
        System.out.println("Task 7: " + number);

        //task 8: Cho một chuỗi str, chuyển các ký tự được chỉ định sang một ký tự khác cho trước
        String str8 = "devcamp java";
        str8 = str8.replace('a', 'b');
        System.out.println("Task 8: " +str8);

        //task 9: Đảo ngược các ký tự của chuỗi cách nhau bởi dấu cách
        String strT9 = "I am developer";
        String[] words = strT9.split(" ");
       
        StringBuilder reversedStr = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            reversedStr.append(words[i]);
            if (i != 0) {
                reversedStr.append(" ");
            }
        }
        System.out.println("Chuoi sau khi dao nguoc: " + reversedStr);

        //task 10: Kiểm tra chuỗi sau khi đảo ngược và chuỗi ban đầu hoàn toàn giống nhau
        String str10 = "aba";
        String str11 = "abv";
        task10(str10);
        task10(str11);



    }

    // task 1:
    public static Set<Character> task1(String str) {
        String x = str.replaceAll("\\s", "");
        char[] result = x.toCharArray();
        Set<Character> duplicates = new HashSet<>();
        for (char a : result) {
            if (str.indexOf(a) != str.lastIndexOf(a)) {
                duplicates.add(a);

            }
        }
        if (duplicates.isEmpty()) {
            System.out.println("NO");
        }

        return duplicates;

    }
    //stask 2
    public static void task2(String s1, String s2){
            boolean isReverse = true;
            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt(i) != s2.charAt(s2.length() - 1 - i)) {
                    isReverse = false;
                    break;
                }
            }
            if (isReverse) {
                System.out.println("OK");
            } else {
                System.out.println("KO");
            }
        
    }
    //stask 3
    public static String findFirstUniqueCharacter(String input) {
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (input.indexOf(c) == input.lastIndexOf(c)) {
                return Character.toString(c);
            }
        }
        return "NO";
    }
    // task 5
    public static boolean task5(String str) {
            Boolean checkNumber = false;
            char[] a = str.toCharArray();
            // for(int i = 0; i < str.length(); i++){
            //     if(Character.isDigit(str.charAt(i))){
            //         checkNumber = true;
            //     }
            // }
            for(char abc : a){
                if(Character.isDigit(abc)){
                    checkNumber = true;
                }
            }
        return checkNumber;
    }
    //task 6 đếm co bao nhieu nguyen am
    public static void task6(String str){
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                count++;
            }
        }
        System.out.println("Task 6: Chuoi co " + count + " nguyen am.");
    }

     //task 10
     public static void task10(String str){
        boolean isPalindrome = true;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != str.charAt(str.length() - i - 1)) {
                isPalindrome = false;
                break;
            }
        }
        System.out.println("Kiem tra chuoi co dao nguoc khong: " + isPalindrome);
    }


}
